
import java.util.GregorianCalendar;
import java.util.Date;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Daniele Claudine Muller
 */
public class Pratica31 {
    private static Date inicio = new Date();
    private static String meuNome = "DANIELE CLAUDINE MULLER";
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1990,10,19); 
    private static GregorianCalendar hoje = new GregorianCalendar(); 
    
    public static void main(String[] args) {
      
        long diferenca = (hoje.getTimeInMillis() - dataNascimento.getTimeInMillis()) / 1000 / 3600 / 24;
        
        System.out.println(meuNome.toUpperCase());
        System.out.println(meuNome.toUpperCase().charAt(17)+meuNome.substring(18, 23).toLowerCase()+", "+
                meuNome.toUpperCase().charAt(0)+". "+meuNome.toUpperCase().charAt(8)+".");
        System.out.println(diferenca);
        
        Date fim = new Date();
        
        System.out.println(fim.getTime() - inicio.getTime());
        
    }
}

